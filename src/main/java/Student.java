public class Student {

    private String name;
    private int age;

    public Student() {
        name = "Bob";
        age = 42;
    }

    @Override
    public String toString() {
        return "Student: " + name + " is " + age;
    }

    public void sayHello() {
        System.out.println("Hello!");
    }

    public void suffer() {
        System.err.println("Has too much work to talk!");
    }
}
